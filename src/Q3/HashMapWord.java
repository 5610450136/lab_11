package Q3;
import java.util.HashMap;


public class HashMapWord {
	String msg;
	HashMap<String,Integer> wordCount;
	public HashMapWord(String s) {
		wordCount = new HashMap<String,Integer>();
		msg = s;
	}
	public void count(){
		String[] ss = msg.split(" ");
		for (String a : ss){
			if (wordCount.containsKey(a) == false) {
			wordCount.put(a,1);
			}
			else {
				int i = wordCount.get(a);
				wordCount.remove(a);
				wordCount.put(a, i+1);
			}
		}
		
	}
	public int hasWord(String word) {
		if (wordCount.containsKey(word) == true) {
		int i = wordCount.get(word);
		return i;
		}
		return 0;
	}
}
